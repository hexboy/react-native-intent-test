import React, {useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  useColorScheme,
  NativeModules,
  View,
} from 'react-native';
import SendIntentAndroid from 'react-native-send-intent';
import ActivityResult from 'react-native-activity-result';
const {MyActivityModule} = NativeModules;

import {Colors, Header} from 'react-native/Libraries/NewAppScreen';

const Section = ({children, title}) => {
  const isDarkMode = useColorScheme() === 'dark';
  return (
    <View style={styles.sectionContainer}>
      <Text
        style={[
          styles.sectionTitle,
          {
            color: isDarkMode ? Colors.white : Colors.black,
          },
        ]}>
        {title}
      </Text>
      <Text
        style={[
          styles.sectionDescription,
          {
            color: isDarkMode ? Colors.light : Colors.dark,
          },
        ]}>
        {children}
      </Text>
    </View>
  );
};

const App = () => {
  const isDarkMode = useColorScheme() === 'dark';
  const [name, setName] = useState('hex');
  const [age, setAge] = useState('20');

  const backgroundStyle = {
    backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,
  };

  return (
    <SafeAreaView style={backgroundStyle}>
      <StatusBar barStyle={isDarkMode ? 'light-content' : 'dark-content'} />
      <ScrollView
        contentInsetAdjustmentBehavior="automatic"
        style={backgroundStyle}>
        <Header />
        <View
          style={{
            backgroundColor: isDarkMode ? Colors.black : Colors.white,
          }}>
          <View style={styles.forminput}>
            <Text>Name: </Text>
            <TextInput
              value={name}
              style={styles.input}
              onChangeText={val => {
                setName(val);
              }}
            />
          </View>
          <View style={styles.forminput}>
            <Text>Age: </Text>
            <TextInput
              style={styles.input}
              value={age}
              onChangeText={val => {
                setAge(val);
              }}
            />
          </View>
          <TouchableOpacity
            onPress={async () => {
              // console.log({MyActivityModule});
              const isInstalled = await MyActivityModule.isAppInstalled(
                'com.appa',
              );
              if (!isInstalled) {
                return console.error('app not installed');
              }

              try {
                const response = await MyActivityModule.startActivityForResult(
                  1001,
                  {
                    packageName: 'com.appa',
                    category: 'android.intent.category.LAUNCHER',
                    extra: {
                      name,
                      age,
                    },
                  },
                );

                console.log({response});
                if (response.resultCode !== MyActivityModule.OK) {
                  throw new Error('Invalid result from activity.');
                } else {
                  console.log('Got the following response: ' + response.data);
                }
              } catch (e) {
                console.error(e);
              }
            }}>
            <Section title="Lunch AppA">
              open <Text style={styles.highlight}>appa </Text>
              with init props, MyActivityModule
            </Section>
          </TouchableOpacity>

          <TouchableOpacity
            onPress={async () => {
              const response = await SendIntentAndroid.openApp('com.appa', {
                name,
                age,
              });
              console.log({response});
              if (response.resultCode !== SendIntentAndroid.OK) {
                throw new Error('Invalid result from activity.');
              } else {
                console.log('Got the following response: ' + response.data);
              }
            }}>
            <Section title="Lunch AppA">
              open <Text style={styles.highlight}>appa </Text>
              with init props SendIntentAndroid
            </Section>
          </TouchableOpacity>

          <TouchableOpacity
            onPress={async () => {
              const response = await ActivityResult.startActivityForResult(
                1003,
                'com.appa/.MainActivity',
                {
                  name,
                  age,
                },
              );
              console.log({response});
              if (response.resultCode !== ActivityResult.OK) {
                throw new Error('Invalid result from activity.');
              } else {
                console.log('Got the following response: ' + response.data);
              }
            }}>
            <Section title="Lunch AppA">
              open <Text style={styles.highlight}>appa </Text>
              with init props ActivityResult
            </Section>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
  },
  highlight: {
    fontWeight: '700',
  },
  forminput: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: '10%',
  },
  input: {
    borderColor: '#ffbf00',
    borderWidth: 1,
    width: '50%',
    borderRadius: 5,
    marginTop: 10,
    paddingHorizontal: 10,
  },
});

export default App;
