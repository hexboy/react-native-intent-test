package com.appb;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.util.SparseArray;

import androidx.annotation.Nullable;

import com.facebook.react.bridge.ActivityEventListener;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.BaseActivityEventListener;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.ReadableArray;
import com.facebook.react.bridge.ReadableMap;
import com.facebook.react.bridge.ReadableMapKeySetIterator;
import com.facebook.react.bridge.ReadableType;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.bridge.WritableNativeMap;

public class MyActivityModule extends ReactContextBaseJavaModule implements ActivityEventListener {
    private static final String E_CANCELLED = "E_CANCELLED";
    private static final String E_ACTIVITY_DOES_NOT_EXIST = "E_ACTIVITY_DOES_NOT_EXIST";
    private static final String E_FAILED_TO_START_ACTIVITY = "E_FAILED_TO_START_ACTIVITY";

    final SparseArray<Promise> mPromises;


    MyActivityModule(ReactApplicationContext reactContext) {
        super(reactContext);
        mPromises = new SparseArray<>();
    }

    @Override
    public void initialize() {
        super.initialize();
        getReactApplicationContext().addActivityEventListener(this);
    }

    @Override
    public void onCatalystInstanceDestroy() {
        super.onCatalystInstanceDestroy();
        getReactApplicationContext().removeActivityEventListener(this);
    }


    @Override
    public void onActivityResult(Activity activity, int requestCode, int resultCode, Intent data) {
        Promise mPromise = mPromises.get(requestCode);
        if (mPromise != null) {
            if (resultCode == Activity.RESULT_CANCELED) {
                mPromise.reject(E_CANCELLED, "activity was cancelled");
            } else if (resultCode == Activity.RESULT_OK) {
                if (data != null) {
                    WritableMap result = new WritableNativeMap();
                    result.putInt("resultCode", resultCode);
                    result.putMap("data", Arguments.makeNativeMap(data.getExtras()));
                    mPromise.resolve(result);
                } else {
                    mPromise.resolve(null);
                }
            }

            mPromises.put(requestCode, null);
        }
    }

    @Override
    public String getName() {
        return "MyActivityModule";
    }

    private boolean parseExtras(ReadableMap extras, Intent intent) {
        ReadableMapKeySetIterator it = extras.keySetIterator();

        while(it.hasNextKey()) {
            String key = it.nextKey();
            ReadableType type = extras.getType(key);

            switch (type) {
                case Boolean:
                    intent.putExtra(key, extras.getBoolean(key));
                    break;
                case Map:
                    //because in js, there is no distinction between double, int, short, etc. we use an object indicating the type
                    ReadableMap map = extras.getMap(key);
                    if (map.hasKey("type")) {
                        String actualType = map.getString("type").toLowerCase();
                        switch (actualType) {
                            case "int":
                                intent.putExtra(key, map.getInt("value"));
                                break;
                            case "short":
                                intent.putExtra(key, (short)map.getInt("value"));
                                break;
                            case "byte":
                                intent.putExtra(key, (byte)map.getInt("value"));
                                break;
                            case "char":
                                intent.putExtra(key, (char)map.getInt("value"));
                                break;
                            case "long":
                                intent.putExtra(key, (long)map.getDouble("value"));
                                break;
                            case "float":
                                intent.putExtra(key, (float)map.getDouble("value"));
                                break;
                            case "double":
                                intent.putExtra(key, map.getDouble("value"));
                                break;
                        }
                    }
                    else { //not parsing real maps for now
                        return false;
                    }
                    break;
                case Number:
                    intent.putExtra(key, (double) extras.getDouble(key));
                    break;
                case String:
                    intent.putExtra(key, extras.getString(key));
                    break;
                case Array:
                    ReadableArray array = extras.getArray(key);
                    if (array.size() == 0) { //cannot guess the type of an empty array
                        return false;
                    }

                    //try to infer the type of the array from the first element
                    ReadableType arrayType = array.getType(0);
                    switch (arrayType) {
                        case Boolean:
                            boolean[] bArray = new boolean[array.size()];
                            for (int i = 0; i < array.size(); ++i)
                                bArray[i] = array.getBoolean(i);
                            intent.putExtra(key, bArray);
                            break;
                        case Map:
                            ReadableMap aMap = extras.getMap(key);
                            if (aMap.hasKey("type")) {
                                String actualType = aMap.getString("type").toLowerCase();
                                switch (actualType) {
                                    case "int":
                                        int[] iArray = new int[array.size()];
                                        for (int i = 0; i < array.size(); ++i)
                                            iArray[i] = array.getInt(i);
                                        intent.putExtra(key, iArray);
                                        break;
                                    case "short":
                                        short[] shArray = new short[array.size()];
                                        for (int i = 0; i < array.size(); ++i)
                                            shArray[i] = (short)array.getInt(i);
                                        intent.putExtra(key, shArray);
                                        break;
                                    case "byte":
                                        byte[] byArray = new byte[array.size()];
                                        for (int i = 0; i < array.size(); ++i)
                                            byArray[i] = (byte)array.getInt(i);
                                        intent.putExtra(key, byArray);
                                        break;
                                    case "char":
                                        char[] cArray = new char[array.size()];
                                        for (int i = 0; i < array.size(); ++i)
                                            cArray[i] = (char)array.getInt(i);
                                        intent.putExtra(key, cArray);
                                        break;
                                    case "long":
                                        long[] lArray = new long[array.size()];
                                        for (int i = 0; i < array.size(); ++i)
                                            lArray[i] = (long)array.getInt(i);
                                        intent.putExtra(key, lArray);
                                        break;
                                    case "float":
                                        float[] fArray = new float[array.size()];
                                        for (int i = 0; i < array.size(); ++i)
                                            fArray[i] = (float)array.getDouble(i);
                                        intent.putExtra(key, fArray);
                                        break;
                                    case "double":
                                        double[] doArray = new double[array.size()];
                                        for (int i = 0; i < array.size(); ++i)
                                            doArray[i] = array.getDouble(i);
                                        intent.putExtra(key, doArray);
                                        break;
                                }
                            }
                            else { //not parsing real maps for now
                                return false;
                            }
                            break;
                        case Number:
                            double[] dArray = new double[array.size()];
                            for (int i = 0; i < array.size(); ++i)
                                dArray[i] = array.getDouble(i);
                            intent.putExtra(key, dArray);
                            break;
                        case String:
                            String[] sArray = new String[array.size()];
                            for (int i = 0; i < array.size(); ++i)
                                sArray[i] = array.getString(i);
                            intent.putExtra(key, sArray);
                            break;
                    }

                    break;
                  //ignore everything else
            }
        }

        return true;
    }

    @ReactMethod
    public void openApp(String packageName, ReadableMap extras, final Promise promise) {
        Activity currentActivity = getCurrentActivity();

        if (currentActivity == null) {
            promise.reject(E_ACTIVITY_DOES_NOT_EXIST, "Activity doesn't exist");
            return;
        }

        Intent sendIntent = currentActivity.getPackageManager().getLaunchIntentForPackage(packageName);

        if (sendIntent == null) {
            promise.resolve(false);
            return;
        }

        if (!parseExtras(extras, sendIntent)) {
            promise.resolve(false);
            return;
        }

        sendIntent.addCategory(Intent.CATEGORY_LAUNCHER);
        currentActivity.startActivity(sendIntent);
        promise.resolve(true);
    }

    @ReactMethod
    public void startActivityForResult(int requestCode, String packageName, ReadableMap extras, final Promise promise) {
        Activity currentActivity = getCurrentActivity();

        if (currentActivity == null) {
            promise.reject(E_ACTIVITY_DOES_NOT_EXIST, "Activity doesn't exist");
            return;
        }

        // Store the promise to resolve/reject when picker returns data
        mPromises.put(requestCode, promise);

        Intent sendIntent = currentActivity.getPackageManager().getLaunchIntentForPackage(packageName);

        if (sendIntent == null) {
            promise.resolve(false);
            return;
        }

        promise.resolve(false);

        if (!parseExtras(extras, sendIntent)) {
            promise.resolve(false);
            return;
        }

        try {
            sendIntent.addCategory(Intent.CATEGORY_LAUNCHER);
            currentActivity.startActivityForResult(sendIntent, requestCode);
        } catch (Exception e) {
            Promise mPromise = mPromises.get(requestCode);
            mPromise.reject(E_FAILED_TO_START_ACTIVITY, e);
            mPromises.put(requestCode, null);
        }
    }

    @ReactMethod
    public void resolveActivity(String action, Promise promise) {
        Activity activity = getCurrentActivity();
        Intent intent = new Intent(action);
        ComponentName componentName = intent.resolveActivity(activity.getPackageManager());
        if (componentName == null) {
            promise.resolve(null);
            return;
        }

        WritableMap map = new WritableNativeMap();
        map.putString("class", componentName.getClassName());
        map.putString("package", componentName.getPackageName());
        promise.resolve(map);
    }

    @Override
    public void onNewIntent(Intent intent) {
        /* Do nothing */
    }
}