package com.appb;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.SparseArray;

import com.facebook.react.bridge.ActivityEventListener;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.BaseActivityEventListener;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.ReadableMap;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.bridge.WritableNativeMap;

public class MyActivityModule extends ReactContextBaseJavaModule  {
    private static final String E_CANCELLED = "E_CANCELLED";
    private static final String E_FAILED_TO_START_ACTIVITY = "E_FAILED_TO_START_ACTIVITY";
    private static final String ATTR_ACTION = "action";
    private static final String ATTR_TYPE = "type";
    private static final String ATTR_CATEGORY = "category";
    private static final String TAG_EXTRA = "extra";
    private static final String ATTR_DATA = "data";
    private static final String ATTR_FLAGS = "flags";
    private static final String ATTR_PACKAGE_NAME = "packageName";
    private static final String ATTR_CLASS_NAME = "className";

    final SparseArray<Promise> mPromises = new SparseArray<>();

    private final ActivityEventListener mActivityEventListener = new BaseActivityEventListener() {
        @Override
        public void onActivityResult(Activity activity, int requestCode, int resultCode, Intent intent) {
            Promise mPromise = mPromises.get(requestCode);
            if (mPromise != null) {
                if (resultCode == Activity.RESULT_CANCELED) {
                    mPromise.reject(E_CANCELLED, "activity was cancelled");
                } else if (resultCode == Activity.RESULT_OK) {
                    if (intent != null) {
                        WritableMap result = new WritableNativeMap();
                        result.putInt("resultCode", resultCode);
//                    result.putMap("data", Arguments.makeNativeMap(intent.getExtras()));

                        Uri data = intent.getData();
                        if (data != null) {
                            result.putString("data", data.toString());
                        }

                        Bundle extras = intent.getExtras();
                        if (extras != null) {
                            result.putMap("extra", Arguments.fromBundle(extras));
                        }
                        mPromise.resolve(result);
                    } else {
                        mPromise.resolve(null);
                    }
                }

                mPromises.remove(requestCode);
            }
        }
    };

    MyActivityModule(ReactApplicationContext reactContext) {
        super(reactContext);

        // Add the listener for `onActivityResult`
        reactContext.addActivityEventListener(mActivityEventListener);
    }

    @Override
    public String getName() {
        return "MyActivityModule";
    }

    @ReactMethod
    public void startActivityForResult(int requestCode, ReadableMap params, final Promise promise) {
        Activity currentActivity = getCurrentActivity();

        // Store the promise to resolve/reject when returns data
        mPromises.put(requestCode, promise);

        Intent intent = new Intent();

        if (params.hasKey(ATTR_PACKAGE_NAME)) {
            intent = currentActivity.getPackageManager().getLaunchIntentForPackage(params.getString(ATTR_PACKAGE_NAME));
        }

        if (params.hasKey(ATTR_CLASS_NAME)) {
            ComponentName cn;
            if (params.hasKey(ATTR_PACKAGE_NAME)) {
                cn = new ComponentName(params.getString(ATTR_PACKAGE_NAME), params.getString(ATTR_CLASS_NAME));
            } else {
                cn = new ComponentName(getReactApplicationContext(), params.getString(ATTR_CLASS_NAME));
            }
            intent.setComponent(cn);
        }
        if (params.hasKey(ATTR_ACTION)) {
            intent.setAction(params.getString(ATTR_ACTION));
        }
        // setting data resets type; and setting type resets data; if you have both, you need to set them at the same time
        // https://developer.android.com/guide/components/intents-filters.html#Types (see 'Data' section)
        if (params.hasKey(ATTR_DATA) && params.hasKey(ATTR_TYPE)) {
            intent.setDataAndType(Uri.parse(params.getString(ATTR_DATA)), params.getString(ATTR_TYPE));
        } else {
            if (params.hasKey(ATTR_DATA)) {
                intent.setData(Uri.parse(params.getString(ATTR_DATA)));
            }
            if (params.hasKey(ATTR_TYPE)) {
                intent.setType(params.getString(ATTR_TYPE));
            }
        }
        if (params.hasKey(TAG_EXTRA)) {
            intent.putExtras(Arguments.toBundle(params.getMap(TAG_EXTRA)));
        }
        if (params.hasKey(ATTR_FLAGS)) {
            intent.addFlags(params.getInt(ATTR_FLAGS));
        }
        if (params.hasKey(ATTR_CATEGORY)) {
            intent.addCategory(params.getString(ATTR_CATEGORY));
        }

        try {
            currentActivity.startActivityForResult(intent, requestCode, null);
        } catch (Exception e) {
            promise.reject(E_FAILED_TO_START_ACTIVITY, e);
            mPromises.remove(requestCode);
        }
    }

    @ReactMethod
    public void isAppInstalled(String packageName, final Promise promise) {
        Activity currentActivity = getCurrentActivity();
        try {
            currentActivity.getPackageManager().getPackageInfo(packageName, 0);
        } catch (PackageManager.NameNotFoundException e) {
            promise.resolve(false);
            return;
        }
        promise.resolve(true);
    }

    @ReactMethod
    public void finish(int resultCode, ReadableMap map) {
        Activity activity = getCurrentActivity();
        Intent intent = new Intent();
        intent.putExtras(Arguments.toBundle(map));
        activity.setResult(resultCode, intent);
        activity.finish();
    }
}