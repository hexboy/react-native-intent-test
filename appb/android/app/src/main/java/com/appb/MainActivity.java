package com.appb;

import android.app.Activity;
import android.os.Bundle;

import androidx.annotation.Nullable;

import com.facebook.react.ReactActivity;
import com.facebook.react.ReactActivityDelegate;

import java.util.Set;

public class MainActivity extends ReactActivity {

  /**
   * Returns the name of the main component registered from JavaScript. This is used to schedule
   * rendering of the component.
   */
  @Override
  protected String getMainComponentName() {
    return "appb";
  }

  @Override
  protected ReactActivityDelegate createReactActivityDelegate() {
    return new InitialPropsReactActivityDelegate(this, getMainComponentName());
  }

  public static class InitialPropsReactActivityDelegate extends ReactActivityDelegate {
    private final @Nullable Activity mActivity;
    private @Nullable Bundle mInitialProps = null;

    public InitialPropsReactActivityDelegate(ReactActivity activity, @Nullable String mainComponentName) {
      super(activity, mainComponentName);
      this.mActivity = activity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
      mInitialProps = mActivity.getIntent().getExtras();
      super.onCreate(null);
    }

    @Override
    protected @Nullable Bundle getLaunchOptions() {
      if(mInitialProps != null) {
        Set<String> keys = mInitialProps.keySet();
        if(keys.contains("profile") && mInitialProps.get("profile").getClass().getName().equals("android.os.UserHandle")) {
          return null;
        }
      }
      return mInitialProps;
    }
  }
}
